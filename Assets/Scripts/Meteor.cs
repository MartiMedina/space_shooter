﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    Vector2 speed;

    public GameObject[] Graphics;
    int seleccionado;
    public AudioSource audioSource;

	public ParticleSystem ps;

    void Awake()
    {


        for (int i = 0; i < Graphics.Length; i++)
        {
            Graphics[i].SetActive(false);
        }

        seleccionado = Random.Range(0, Graphics.Length);
        Graphics[seleccionado].SetActive(true);


        speed.x = Random.Range (-4, 0);
        speed.y = Random.Range (-4, 5);

    }

    void Update()
    {
        transform.Translate(speed * Time.deltaTime);

        Graphics[seleccionado].transform.Rotate(0, 0, 100 * Time.deltaTime);
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Finish")
        {
            Destroy(this.gameObject);
        }
        else if (other.tag == "Bullet")
        {
            StartCoroutine(DestroyMeteor());
        }
    }

    IEnumerator DestroyMeteor(){
        //Desactivo el grafico
        Graphics[seleccionado].SetActive(false);

		ps.Play();

		Destroy(GetComponent<BoxCollider2D>());
        //Lanzo sonido de explosion
        audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Me destruyo a mi mismo
        Destroy(this.gameObject);
    }
}