﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    private Vector2 speed = new Vector2(0, 0);

    private void Start()
    {
        StartCoroutine(EnemyBehaviour());
    }

    void Update()
    {
         transform.Translate(speed * Time.deltaTime);
    }

    IEnumerator EnemyBehaviour()
    {
        while (true)
        {
            //Avanza horizontalmente
            speed.x = -1f;

            yield return new WaitForSeconds(1.0f);

            //Se para
            speed.x = 0f;

            yield return new WaitForSeconds(1.0f);

            //Dispara
        }
    }
}
